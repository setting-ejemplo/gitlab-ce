# README #

## GITLAB-CE ##

### Clonar repositorio ###
* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/gitlab-ce.git

### Crear imagen
* docker build -t settingejemplo/gitlab-ce .

### Crear contenedor ###
* docker run \
  --detach \
  --name gitlab-ce-container \
  --restart always \
  --hostname setting.gitlab.dev \
  --network setting \
  --ip 172.13.1.10 \
  --publish 443:443 \
  --publish 80:80 \
  --publish 2022:22 \
  --volume /srv/gitlab-ce/config:/etc/gitlab \
  --volume /srv/gitlab-ce/logs:/var/log/gitlab \
  --volume /srv/gitlab-ce/data:/var/opt/gitlab \
  settingejemplo/gitlab-ce
